const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Course
var indonesia_sfa_information = new Schema({

  "distributor_sfa_code" : {type: String},
  "distributor_shell_ref_code" : {type: String},
  "distributor_name" : {type: String},
  "dsr_code" : {type: String},
  "visit_date" : {type: String},
  "visit_status" : {type: String},
  "outlet_code" : {type: String},
  "outlet_sector" : {type: String},
  "visit_latitude" : {type: String},
  "visit_longitude" : {type: String},
  "total_order_volume" : {type: Number},
  "premium_order_volume" : {type: Number},
  "reference_key" : {type: String},
  "country_id" : {type: String},
  "visit_date_processed" : {type: String},
  "status":{type:String,default:"Entered"},
  "process":{type: String}
  "StFl":{type: String}
},{
    collection: 'indonesia_sfa_information_2019'
});

module.exports = mongoose.model('indonesia_sfa_information', indonesia_sfa_information);